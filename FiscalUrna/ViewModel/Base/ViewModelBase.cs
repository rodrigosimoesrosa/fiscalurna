﻿using FiscalUrna.View;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace FiscalUrna.ViewModel.Base
{
    
    public abstract class ViewModelBase <T> where T : PhoneApplicationPage {

        public T Page { get; set; }

        public ViewModelBase(T page)
        {
            Page = page;
        }
    }
}
