﻿using FiscalUrna.Model;
using FiscalUrna.Page.Photo;
using FiscalUrna.Util;
using FiscalUrna.ViewModel.Base;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FiscalUrna.ViewModel.Photo
{
    public class FiscalUrnaPhotoViewModel : ViewModelBase<FiscalUrnaPhotoPage>
    {
        public BitmapImage Bitmap { get; private set; }

        private CameraCaptureTask capture;

        public FiscalUrnaPhotoViewModel(FiscalUrnaPhotoPage Page) : base(Page) {}


        private void OnCaptureCompleted(object sender, PhotoResult e)
        {

            if (e.Error == null)
            {
                if (e.ChosenPhoto == null)
                {
                    if (Bitmap == null)
                    {
                        Page.ShowAlert();
                    }
                }
                else
                {
                    Bitmap = new BitmapImage();
                    Bitmap.SetSource(e.ChosenPhoto);

                    FiscalUrnaManager.Data.Bitmap = Bitmap;

                    Page.DataContext = FiscalUrnaManager.Data;
                    Page.OnSuccessCaptureImage();
                }
            }
            else
            {
                if (Bitmap == null)
                {
                    Page.ShowAlert();
                }
            }
        }

        public void CapturePhoto()
        {
            capture = new CameraCaptureTask();
            capture.Completed += OnCaptureCompleted;
            capture.Show();
        }

        public Complain GetData()
        {
            return FiscalUrnaManager.Data;
        }
    }
}
