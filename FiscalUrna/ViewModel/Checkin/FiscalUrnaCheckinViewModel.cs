﻿using FiscalUrna.Model;
using FiscalUrna.Page.Checkin;
using FiscalUrna.Util;
using FiscalUrna.Util.Settings;
using FiscalUrna.ViewModel.Base;
using Microsoft.Phone.Maps.Services;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;

namespace FiscalUrna.ViewModel.Checkin
{
    public class FiscalUrnaCheckinViewModel : ViewModelBase<FiscalUrnaCheckinPage>
    {
        public GeoCoordinate Coordinate { get; private set; }
        private ReverseGeocodeQuery GeocodeQuery { get; set; }
        public MapAddress Address { get; private set; }

        public Geoposition Position { get; private set; }

        public FiscalUrnaCheckinViewModel(FiscalUrnaCheckinPage Page) : base(Page) { }

        public async void GetPosition()
        {
            if (!FiscalUrnaAppStorageSettings.IsAcceptGetLocation())
            {
                return;
            }

            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 1;

            try
            {
                Position = await geolocator.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
                Coordinate = new GeoCoordinate(Position.Coordinate.Latitude, Position.Coordinate.Longitude);

                GeocodeQuery = new ReverseGeocodeQuery();
                GeocodeQuery.GeoCoordinate = Coordinate;
                GeocodeQuery.QueryCompleted += OnGeocodeQueryCompleted;
                GeocodeQuery.QueryAsync();
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x80004004)
                {
                    Page.ShowMessageBox("Habilite a opcão de localização no aparelho!");
                }
            }
        }

        private void OnGeocodeQueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null && e.Result.Count > 0)
            {
                Address = e.Result[0].Information.Address;

                #region Seta a opção escolhida.
                FiscalUrnaManager.Data.Coordinate = Coordinate;
                FiscalUrnaManager.Data.Address = Address;
                FiscalUrnaManager.Data.Time = DateTime.Now;

                Page.OnSuccessLocation();
                #endregion
            }
            else
            {
                Page.ShowMessageBox("Ocorreu um erro ao obter a localização!");
            }
        }

        public void SetDescription(string description)
        {
            FiscalUrnaManager.Data.Description = description;
        }

        public Complain GetData()
        {
            return FiscalUrnaManager.Data;
        }
    }
}
