﻿using FiscalUrna.Model;
using FiscalUrna.Page;
using FiscalUrna.Page.Sign;
using FiscalUrna.Page.Util;
using FiscalUrna.Util;
using FiscalUrna.Util.Json;
using FiscalUrna.Util.Json.Request;
using FiscalUrna.Util.Settings;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace FiscalUrna.ViewModel
{
    public class FiscalUrnaSignViewModel : ViewModelBase<FiscalUrnaSignPage>, HandlerDownload<User>
    {

        private bool debug = true;
        private  DispatcherTimer timer;

        public User User { get; set; }

        public FiscalUrnaSignViewModel(FiscalUrnaSignPage Page) : base(Page) { }

        private FiscalUrnaSignRequest Request { get; set; }

        public void Register(string mail, string password, string name)
        {
            User = new User();
            User.Mail = mail;
            User.Password = password;
            User.Name = name;

            if (debug)
            {
                timer = new DispatcherTimer();

                timer.Interval = TimeSpan.FromMilliseconds(5000);
                timer.Tick +=new EventHandler(FinishTimer);
                timer.Start();

            }else{
                Request = new FiscalUrnaSignRequest(this);
                Request.Execute();
            }
        }

        private void FinishTimer(object sender, EventArgs e){
            timer.Stop();

            if (!FiscalUrnaAppStorageSettings.IsRegistered())
            {
                FiscalUrnaAppStorageSettings.AddUser(User);
                FiscalUrnaManager.User = User;
                Page.OnAccept();
            }
        }

        public void OnFinishDownload(User User)
        {
            if (!FiscalUrnaAppStorageSettings.IsRegistered())
            {
                FiscalUrnaAppStorageSettings.AddUser(User);
                FiscalUrnaManager.User = User;
                Page.OnAccept();
            }
        }

        public void OnErrorDownload(string error)
        {
            Page.OnRefused(error);
        }

        public User GetUser()
        {
            return FiscalUrnaManager.User;
        }

        public void OnRemoveUser()
        {
            FiscalUrnaAppStorageSettings.RemoveUser();
        }
    }
}
