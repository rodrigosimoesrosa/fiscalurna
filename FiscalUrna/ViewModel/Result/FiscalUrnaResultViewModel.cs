﻿using FiscalUrna.Model;
using FiscalUrna.Page;
using FiscalUrna.Util;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FiscalUrna.ViewModel.Result
{
    public class FiscalUrnaResultViewModel : ViewModelBase<FiscalUrnaResultPage>
    {
        private DispatcherTimer timer;

        public FiscalUrnaResultViewModel(FiscalUrnaResultPage Page) : base(Page) { }

        public Complain GetData()
        {
            return FiscalUrnaManager.Data;
        }

        public void Publish()
        {
            if (App.Debug)
            {
                timer = new DispatcherTimer();

                timer.Interval = TimeSpan.FromMilliseconds(5000);
                timer.Tick += new EventHandler(FinishTimer);
                timer.Start();
            }
        }

        private void FinishTimer(object sender, EventArgs e)
        {
            timer.Stop();
            FiscalUrnaManager.Data = null;
            Page.OnPublishSuccess();
            //Page.OnPublishFail();
        }
    }
}
