﻿using FiscalUrna.Model;
using FiscalUrna.Page.Login;
using FiscalUrna.Page.Util;
using FiscalUrna.Util;
using FiscalUrna.Util.Request.User;
using FiscalUrna.Util.Settings;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FiscalUrna.ViewModel.Login
{
    public class FiscalUrnaLoginViewModel : ViewModelBase<FiscalUrnaLoginPage>, HandlerDownload<User>
    {
        public User DebugUser { get; set; }

        private DispatcherTimer timer;

        public FiscalUrnaLoginViewModel(FiscalUrnaLoginPage Page) : base(Page) { }

        private FiscalUrnaLoginRequest Request { get; set; }

        
        private void FinishTimer(object sender, EventArgs e)
        {
            timer.Stop();
            FiscalUrnaAppStorageSettings.AddUser(DebugUser);
            FiscalUrnaManager.User = DebugUser;
            Page.OnSuccess();
        }

        public void OnFinishDownload(User User)
        {
            FiscalUrnaAppStorageSettings.AddUser(User);
            FiscalUrnaManager.User = User;
            Page.OnSuccess();
        }

        public void OnErrorDownload(string error)
        {
            Page.OnFail(error);
        }

        public void Login(string mail, string password)
        {
            if (App.Debug)
            {
                DebugUser = new User();
                DebugUser.Mail = mail;
                DebugUser.Password = password;
                DebugUser.Id = 1;
                DebugUser.Name = "Rodrigo";

                timer = new DispatcherTimer();

                timer.Interval = TimeSpan.FromMilliseconds(5000);
                timer.Tick += new EventHandler(FinishTimer);
                timer.Start();
            }
            else
            {
                Request = new FiscalUrnaLoginRequest(this);
                Request.Execute();
            }
        }
    }
}
