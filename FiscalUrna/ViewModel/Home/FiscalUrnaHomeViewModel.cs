﻿using FiscalUrna.Model;
using FiscalUrna.Page.Home;
using FiscalUrna.Util;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.ViewModel.Home
{
    public class FiscalUrnaHomeViewModel : ViewModelBase<FiscalUrnaHomePage>
    {

        public FiscalUrnaHomeViewModel(FiscalUrnaHomePage Page) : base(Page) { }

        public ObservableCollection<Complain> GetListComplainMockup(int num)
        {
            ObservableCollection<Complain> list = new ObservableCollection<Complain>();
            for (int i = 0; i < num; i++ )
            {
                Complain c = null;

                Complain data = FiscalUrnaManager.Data;
                if (data != null)
                {
                    c = new Complain(data.Type);
                    c.Address = data.Address;
                    c.Bitmap = data.Bitmap;
                    c.Coordinate = data.Coordinate;
                    c.Description = data.Description;
                    c.Time = data.Time;
                }
                else
                {
                    bool a = i % 2 == 0;
                    c = new Complain(a ? Complain.ELECTORAL_GARBAGE : Complain.EXIT_POLLS);
                    c.Time = DateTime.Now;
                    c.Id = i;
                    c.Coordinate = new System.Device.Location.GeoCoordinate(37.321503, -122.032572);
                    c.Address = null;
                    c.Bitmap = null;
                    c.Description = "Descrição";
                }

                list.Add(c);
            }

            return list;
        }

        public ObservableCollection<Complain> GetMyComplainMockup()
        {
            return GetListComplainMockup(3);
        }

        public void CreateComplain(string type)
        {
            FiscalUrnaManager.Create(type);
        }
    }
}
