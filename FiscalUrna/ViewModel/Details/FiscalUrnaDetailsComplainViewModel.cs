﻿using FiscalUrna.Model;
using FiscalUrna.Page.Details;
using FiscalUrna.Util.Request.Complain;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FiscalUrna.ViewModel.Details
{
    public class FiscalUrnaDetailsComplainViewModel : ViewModelBase<FiscalUrnaDetailsComplainPage>
    {
        private bool debug = true;
        public DispatcherTimer timer;

        public FiscalUrnaDetailsComplainRequest Request { get; set; }

        public FiscalUrnaDetailsComplainViewModel(FiscalUrnaDetailsComplainPage Page) : base(Page) { }

        public void GetDetailsComplain(int id)
        {
            if (debug)
            {
                timer = new DispatcherTimer();

                timer.Interval = TimeSpan.FromMilliseconds(5000);
                timer.Tick += new EventHandler(FinishTimer);
                timer.Start();
            }
            else
            {
                Request = new FiscalUrnaDetailsComplainRequest(this);
                Request.Execute();
            }
        }

        private void FinishTimer(object sender, EventArgs e)
        {
            Complain c = new Complain(Complain.ELECTORAL_GARBAGE);
            c.Time = DateTime.Now;
            c.Coordinate = new System.Device.Location.GeoCoordinate(37.321503, -122.032572);
            c.Address = null;
            c.Bitmap = null;
            c.Description = "Descrição";
            
            Page.OnSuccessDetailsComplain(c);
        }
    }
}
