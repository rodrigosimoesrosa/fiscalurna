﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.ObjectModel;
using FiscalUrna.ViewModel.Base;
using FiscalUrna.Page.Util;
using FiscalUrna.Model;
using System.Windows.Threading;
using System.IO.IsolatedStorage;
using FiscalUrna.Util.Json;
using System.Windows;
using FiscalUrna.Util;
using FiscalUrna.Util.Settings;

namespace FiscalUrna.ViewModel.Main
{

    public class MainViewModel : ViewModelBase<MainPage>
    {
        private DispatcherTimer  timer;

        public User User { get; set; }

        public MainViewModel(MainPage Page) : base(Page) { }

        public void LoadData()
        {
            timer = new DispatcherTimer();

            timer.Interval = TimeSpan.FromMilliseconds(5000);
            timer.Tick +=new EventHandler(FinishTimer);
            timer.Start();
        }

        private void FinishTimer(object sender, EventArgs e){
            timer.Stop();

            if (FiscalUrnaAppStorageSettings.IsRegistered())
            {
                User = FiscalUrnaAppStorageSettings.GetUser();

                FiscalUrnaManager.User = User;

                Page.OnSuccess();

                //Validar online o usuário
            }
            else
            {
                Page.OnFail();
            }
        }


        public void OnErrorDownload(string error)
        {
            MessageBoxResult result = MessageBox.Show("Ocorreu um erro ao obter as informações do usuário!",
                                   "Alerta", MessageBoxButton.OK);
        }
    }
}
