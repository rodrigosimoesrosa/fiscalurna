﻿using FiscalUrna.Page.Terms;
using FiscalUrna.Util.Settings;
using FiscalUrna.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.ViewModel.Terms
{
    public class FiscalUrnaTermsUseViewModel : ViewModelBase<FiscalUrnaTermsUsePage>
    {
        public FiscalUrnaTermsUseViewModel(FiscalUrnaTermsUsePage Page) : base(Page) { }

        public void AcceptTerms()
        {
            FiscalUrnaAppStorageSettings.AcceptTerms(true);
        }

        public bool IsAcceptTerms()
        {
            return FiscalUrnaAppStorageSettings.isAcceptTerms();
        }

        public void RefuseTerms()
        {
            FiscalUrnaAppStorageSettings.AcceptTerms(false);
        }
    }
}
