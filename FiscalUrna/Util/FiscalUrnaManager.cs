﻿using FiscalUrna.Model;
using FiscalUrna.Util.Json;
using Microsoft.Phone.Maps.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Windows.Devices.Geolocation;

namespace FiscalUrna.Util
{
    public class FiscalUrnaManager
    {
        public static Complain Data { get; set; }

        public static void Create(string type)
        {
            if (Data == null)
            {
                Data = new Complain(type);
            }
        }
        
        public static User User { get;set;}

        public static ObservableCollection<Complain> List { get; set; }

    }
}
