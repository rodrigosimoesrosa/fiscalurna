﻿using FiscalUrna.Model;
using FiscalUrna.Page.Sign;
using FiscalUrna.Page.Util;
using FiscalUrna.Util.Json.Request;
using FiscalUrna.ViewModel;
using FiscalUrna.ViewModel.Base;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util.Json.Request
{
    public class FiscalUrnaSignRequest : FiscalUrnaRequestBase<FiscalUrnaSignPage>
    {
        private HandlerDownload<User> Handler;

        private HttpRequestString Request {get;set;}

        public FiscalUrnaSignRequest(FiscalUrnaSignViewModel viewModel) : base(viewModel)
        {
            Handler = (HandlerDownload<User>)ViewModel;
            Request = new HttpRequestString(FiscalUrnaConstant.SIGN_USER, LinesHandlerCompleted, LinesHandlerChanged);
        }

        public override void Execute()
        {
            Request.execute();
        }

        private void LinesHandlerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null) return;
            try
            {
                User user = new FiscalUrnaJson<User>().ToObject(e.Result);
                Handler.OnFinishDownload(user);
            }
            catch (Exception ex)
            {
                Handler.OnErrorDownload(ex.Message);
            }
        }

        private void LinesHandlerChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }
    }
}
