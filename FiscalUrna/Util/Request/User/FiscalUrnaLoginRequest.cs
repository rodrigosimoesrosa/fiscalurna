﻿using FiscalUrna.Page.Login;
using FiscalUrna.Page.Util;
using FiscalUrna.Util.Json;
using FiscalUrna.Util.Json.Request;
using FiscalUrna.ViewModel.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util.Request.User {

    public class FiscalUrnaLoginRequest : FiscalUrnaRequestBase<FiscalUrnaLoginPage>
    {
        private HandlerDownload<FiscalUrna.Model.User> Handler;

        private HttpRequestString Request { get; set; }

        public FiscalUrnaLoginRequest(FiscalUrnaLoginViewModel viewModel) : base(viewModel)
        {
            Handler = (HandlerDownload<FiscalUrna.Model.User>)ViewModel;
            Request = new HttpRequestString(FiscalUrnaConstant.LOGIN_USER, LinesHandlerCompleted, LinesHandlerChanged);
        }

        public override void Execute()
        {
            Request.execute();
        }

        private void LinesHandlerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null) return;
            try
            {
                FiscalUrna.Model.User user = new FiscalUrnaJson<FiscalUrna.Model.User>().ToObject(e.Result);
                Handler.OnFinishDownload(user);
            }
            catch (Exception ex)
            {
                Handler.OnErrorDownload(ex.Message);
            }
        }

        private void LinesHandlerChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }
    }
}


