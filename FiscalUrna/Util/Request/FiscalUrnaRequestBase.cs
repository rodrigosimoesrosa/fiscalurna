﻿using FiscalUrna.Util;
using FiscalUrna.ViewModel.Base;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FiscalUrna.Util.Json.Request
{
    public abstract class FiscalUrnaRequestBase<T> where T: PhoneApplicationPage
    {
        public ViewModelBase<T> ViewModel { get; set; }

        public FiscalUrnaRequestBase(ViewModelBase<T> viewModel) {
            ViewModel = viewModel;
        }

        public abstract void Execute();
    }
}
