﻿using FiscalUrna.Page.Details;
using FiscalUrna.Page.Login;
using FiscalUrna.Page.Util;
using FiscalUrna.Util.Json;
using FiscalUrna.Util.Json.Request;
using FiscalUrna.ViewModel.Details;
using FiscalUrna.ViewModel.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util.Request.Complain {

    public class FiscalUrnaDetailsComplainRequest : FiscalUrnaRequestBase<FiscalUrnaDetailsComplainPage>
    {
        private HandlerDownload<FiscalUrna.Model.Complain> Handler;

        private HttpRequestString Request { get; set; }

        public FiscalUrnaDetailsComplainRequest(FiscalUrnaDetailsComplainViewModel viewModel) : base(viewModel)
        {
            Handler = (HandlerDownload<FiscalUrna.Model.Complain>)ViewModel;
            Request = new HttpRequestString(FiscalUrnaConstant.DETAILS_COMPLAIN, LinesHandlerCompleted, LinesHandlerChanged);
        }

        public override void Execute()
        {
            Request.execute();
        }

        private void LinesHandlerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null) return;
            try
            {
                FiscalUrna.Model.Complain user = new FiscalUrnaJson<FiscalUrna.Model.Complain>().ToObject(e.Result);
                Handler.OnFinishDownload(user);
            }
            catch (Exception ex)
            {
                Handler.OnErrorDownload(ex.Message);
            }
        }

        private void LinesHandlerChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }
    }
}


