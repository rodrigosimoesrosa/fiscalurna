﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util
{
    public static class FiscalUrnaConstant{
        
        public const string CONNECTION_SQL = @"isostore:/fiscalurna.sdf";

        public static string LOGIN_USER { get { return "http://www.fiscalurna.com.br/services/login"; } }
        public static string SIGN_USER { get { return "http://www.fiscalurna.com.br/services/sign"; } }

        public static string DETAILS_COMPLAIN { get { return "http://www.fiscalurna.com.br/services/details"; } }
    }
}
