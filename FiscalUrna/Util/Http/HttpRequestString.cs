﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util
{
    public class HttpRequestString
    {
        private WebClient webClient;
        private Uri uri;

        public HttpRequestString(String url, DownloadStringCompletedEventHandler completed, DownloadProgressChangedEventHandler changed)
        {
            webClient = new WebClient();
            uri = new Uri(url);
            webClient.DownloadStringCompleted += completed;
            webClient.DownloadProgressChanged += changed;
        }

        public void execute()
        {
            webClient.DownloadStringAsync(uri);
        }
    }
}
