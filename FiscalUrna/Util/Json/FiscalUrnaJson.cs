﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util.Json
{
    public class FiscalUrnaJson<T>
    {
        public T ToObject(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public string ToJson(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
