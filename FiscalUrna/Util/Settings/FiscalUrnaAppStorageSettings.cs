﻿using FiscalUrna.Model;
using FiscalUrna.Util.Json;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util.Settings
{
    public class FiscalUrnaAppStorageSettings
    {
        public static string LOCATION_ACCEPT { get { return "LocationAccept"; } }

        public static string USER { get { return "UserData"; } }

        public static string TERMS_ACCEPT { get { return "TermsAccept"; } }

        public static bool IsRegistered()
        {
            return IsolatedStorageSettings.ApplicationSettings.Contains(USER);
        }

        public static bool IsAcceptGetLocation()
        {
            bool accept = false;
            if (IsolatedStorageSettings.ApplicationSettings.Contains(LOCATION_ACCEPT))
            {
                accept = (bool) IsolatedStorageSettings.ApplicationSettings[LOCATION_ACCEPT];
            }

            return accept;
        }

        public static void AddLocationPermission(bool value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(LOCATION_ACCEPT))
            {
                IsolatedStorageSettings.ApplicationSettings[LOCATION_ACCEPT] = value;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(LOCATION_ACCEPT, value);
            }
            
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static void AddUser(User User)
        {
             string json = new FiscalUrnaJson<User>().ToJson(User);
             IsolatedStorageSettings.ApplicationSettings.Add(USER,json);

             IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static void RemoveUser()
        {
            IsolatedStorageSettings.ApplicationSettings.Remove(USER);
        }

        public static User GetUser()
        {
            string json = IsolatedStorageSettings.ApplicationSettings[USER] as string;
            return new FiscalUrnaJson<User>().ToObject(json);
        }

        public static bool isAcceptTerms()
        {
            bool accept = false;
            if (IsolatedStorageSettings.ApplicationSettings.Contains(TERMS_ACCEPT))
            {
                accept = (bool) IsolatedStorageSettings.ApplicationSettings[TERMS_ACCEPT];
            }

            return accept;
        }

        public static void AcceptTerms(bool value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(TERMS_ACCEPT))
            {
                IsolatedStorageSettings.ApplicationSettings[TERMS_ACCEPT] = value;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(TERMS_ACCEPT, value);
            }
            IsolatedStorageSettings.ApplicationSettings.Save();
        }
    }
}
