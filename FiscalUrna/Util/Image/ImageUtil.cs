﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace FiscalUrna.Util.Image
{
    public class ImageUtil
    {

        public static BitmapImage ImageFromRelativePath(FrameworkElement parent, string path)
        {
            return null;
        }

        public static BitmapImage GetWithoutImage()
        {
            BitmapImage bmp = new BitmapImage();
            bmp.SetSource(Application.GetResourceStream(new Uri(@"Assets/Icons/without_img.png", UriKind.Relative)).Stream);
            return bmp;
        }
    }
}
