﻿using FiscalUrna.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.Util
{
    public static class ServerFix
    {
        public static ObservableCollection<MetroSPLine> Fix(ObservableCollection<MetroSPLine> lines)
        {
            return PutCompanyImage(lines);
        }

        public static ObservableCollection<MetroSPLine> PutCompanyImage(ObservableCollection<MetroSPLine> lines)
        {
            foreach (MetroSPLine line in lines)
            {
                switch (line.IdLine)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 10:
                        line.ImageLine = "/Assets/Icon/metro.png";
                        break;
                    case 17:
                        line.ImageLine = "/Assets/Icon/viaquatro.png";
                        break;
                }
            }

            return lines;
        }
    }
}
