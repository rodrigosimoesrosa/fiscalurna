﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.ViewModel.Details;
using System.Windows.Media;
using FiscalUrna.Model;

namespace FiscalUrna.Page.Details
{
    public partial class FiscalUrnaDetailsComplainPage : PhoneApplicationPage
    {
        private ProgressIndicator Progress;
        public FiscalUrnaDetailsComplainViewModel ViewModel { get; set; }

        public FiscalUrnaDetailsComplainPage()
        {
            InitializeComponent();

            ViewModel = new FiscalUrnaDetailsComplainViewModel(this);
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            
        }

        public void ShowProgressBar()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Buscando denuncia selecionada ...";

            SystemTray.SetProgressIndicator(this, Progress);

            FiscalUrnaDetailsView.Before();
        }

        public void HideProgressBar()
        {
            SystemTray.SetIsVisible(this, false);
            Progress.Text = string.Empty;

            FiscalUrnaDetailsView.After();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string id = string.Empty;
            if (NavigationContext.QueryString.TryGetValue("id", out id))
            {
                ShowProgressBar();
                ViewModel.GetDetailsComplain(Convert.ToInt32(id));
            } 
        }


        public void OnSuccessDetailsComplain(Complain data)
        {
            HideProgressBar(); 
            FiscalUrnaDetailsView.DataContext = data;
            if (data.Bitmap == null)
            {
                FiscalUrnaDetailsView.ImgResult.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}