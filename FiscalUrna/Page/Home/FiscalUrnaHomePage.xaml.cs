﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Util.Settings;
using FiscalUrna.Page.Util;
using FiscalUrna.Page.Sign;
using FiscalUrna.Util;
using FiscalUrna.Model;
using System.Collections.ObjectModel;
using FiscalUrna.ViewModel.Home;

namespace FiscalUrna.Page.Home
{
    public partial class FiscalUrnaHomePage : PhoneApplicationPage
    {
        public FiscalUrnaHomeViewModel ViewModel { get; set; }

        public FiscalUrnaHomePage()
        {
            InitializeComponent();

            ViewModel = new FiscalUrnaHomeViewModel(this);
            Loaded += OnLoadedPage;
        }

        private void OnLoadedPage(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();

            if (!FiscalUrnaAppStorageSettings.IsRegistered())
            {
                ToastManager.GetNewToastImage("Cadastro","Ainda não possui cadastro? Realize quando quiser!").Show();
            }

            FiscalUrnaListComplainView.DataContext = ViewModel.GetListComplainMockup(10);
            FiscalUrnaMyComplainView.DataContext = ViewModel.GetMyComplainMockup();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            NavigationService.RemoveBackEntry();
            FiscalUrnaPivot.SelectedIndex = 0;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Deseja fechar o aplicativo?", "Alerta", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                Application.Current.Terminate();
            }
            else
            {
                e.Cancel = true;
            }
            base.OnBackKeyPress(e);
        }

        private void OnProfileClick(object sender, EventArgs e)
        {
            if (!FiscalUrnaAppStorageSettings.IsRegistered())
            {
                NavigationService.Navigate(new Uri("/Page/Login/FiscalUrnaLoginPage.xaml", UriKind.RelativeOrAbsolute));
            }
            else
            {
                NavigationService.Navigate(new Uri("/Page/Sign/FiscalUrnaSignPage.xaml?edit=true", UriKind.RelativeOrAbsolute));
            }
        }

        private void ShowTerms()
        {
            NavigationService.Navigate(new Uri("/Page/Terms/FiscalUrnaTermsUsePage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void OnTermsUseClick(object sender, EventArgs e)
        {
            ShowTerms();
        }

        public void DoComplain(string type)
        {
            if(FiscalUrnaAppStorageSettings.IsRegistered())
            {
                if (FiscalUrnaAppStorageSettings.isAcceptTerms())
                {
                    bool accept = false;
                    if (FiscalUrnaAppStorageSettings.IsAcceptGetLocation())
                    {
                        accept = true;
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show("Este aplicativo necessita de sua localização. Tudo bem?",
                            "Localização", MessageBoxButton.OKCancel);

                        if (result == MessageBoxResult.OK)
                        {
                            FiscalUrnaAppStorageSettings.AddLocationPermission(true);
                            accept = true;
                        }
                    }

                    if (accept)
                    {
                        ViewModel.CreateComplain(type);
                        NavigationService.Navigate(new Uri("/Page/Checkin/FiscalUrnaCheckinPage.xaml", UriKind.RelativeOrAbsolute));
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("Para realizar uma denuncia você precisa aceitar os termos de utilização do aplicativo, Deseja ler agora ?",
                                                       "Termos de Uso", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        ShowTerms();
                    }
                }

            }else{
                MessageBoxResult result = MessageBox.Show("Para realizar uma denuncia você precisa criar um cadastro, Deseja criar agora ?",
                                   "Cadastro", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    NavigationService.Navigate(new Uri("/Page/Sign/FiscalUrnaSignPage.xaml", UriKind.RelativeOrAbsolute));
                }
            }
        }

        public void ShowDetailsComplain(int id)
        {
            NavigationService.Navigate(new Uri("/Page/Details/FiscalUrnaDetailsComplainPage.xaml?id=" + id, UriKind.RelativeOrAbsolute));
        }
    }
}