﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using FiscalUrna.Util.Image;
using System.Windows.Media.Imaging;
using FiscalUrna.ViewModel.Photo;
using FiscalUrna.Util;
using FiscalUrna.Model;

namespace FiscalUrna.Page.Photo
{
    public partial class FiscalUrnaPhotoPage : PhoneApplicationPage
    {
        public FiscalUrnaPhotoViewModel ViewModel { get; set; }

        public FiscalUrnaPhotoPage()
        {
            InitializeComponent();


            ViewModel = new FiscalUrnaPhotoViewModel(this);
            Loaded += new RoutedEventHandler(OnLoadedPage);
        }



        private void OnLoadedPage(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();
            FiscalUrnaPhotoView.SetImage(ImageUtil.ImageFromRelativePath(this, "/Assets/Icons/without_img.jpg"));
            CapturePhoto();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Você irá perder as informações da denuncia. Deseja continuar ?", "Alerta", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                FiscalUrnaManager.Data = null;
            }

            base.OnBackKeyPress(e);
        }


        public void ShowAlert()
        {
            FiscalUrnaPhotoView.Image.Source = ImageUtil.GetWithoutImage();

            MessageBoxResult result = MessageBox.Show("Ocorreu um erro na recuperação da foto, deseja tentar tirar a foto novamente?", "Foto", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                CapturePhoto();
            }
        }

        public void NextPage()
        {
            NavigationService.Navigate(new Uri("/Page/Result/FiscalUrnaResultPage.xaml", UriKind.RelativeOrAbsolute));
        }

        public void CapturePhoto()
        {
            ViewModel.CapturePhoto();
        }

        public void OnSuccessCaptureImage()
        {
            FiscalUrnaPhotoView.Image.Source = ViewModel.Bitmap;
        }
    }
}