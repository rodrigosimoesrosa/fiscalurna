﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using FiscalUrna.ViewModel.Checkin;
using System.Device.Location;
using System.Windows.Media;
using FiscalUrna.Util.Settings;
using FiscalUrna.View.Util.Map;
using System.Windows.Shapes;
using System.Diagnostics;

namespace FiscalUrna.Page.Checkin
{
    public partial class FiscalUrnaCheckinPage : PhoneApplicationPage
    {
        public FiscalUrnaCheckinViewModel ViewModel { get; set; }

        private ProgressIndicator Progress;

        public FiscalUrnaCheckinPage()
        {
            InitializeComponent();

            ViewModel = new FiscalUrnaCheckinViewModel(this);
        }

        public void ShowProgressBar()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Buscando sua localização ...";

            SystemTray.SetProgressIndicator(this, Progress);
            
            FiscalUrnaCheckinView.BeforeCheckin();
        }

        public void HideProgressBar()
        {
            SystemTray.SetIsVisible(this, false);
            Progress.Text = string.Empty;

            FiscalUrnaCheckinView.AfterCheckin();
        }

        private void OnTouchMarker(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show(ViewModel.Address.Street);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }


        public void NextPage()
        {
            ViewModel.SetDescription(FiscalUrnaCheckinView.TxtDescription.Text);
            MessageBoxResult result = MessageBox.Show("Deseja tirar uma foto como evidência?", "Alerta", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                NavigationService.Navigate(new Uri("/Page/Photo/FiscalUrnaPhotoPage.xaml", UriKind.RelativeOrAbsolute));
            }
            else
            {
                NavigationService.Navigate(new Uri("/Page/Result/FiscalUrnaResultPage.xaml", UriKind.RelativeOrAbsolute));
            }
        }

        public void CheckinClick()
        {
            ShowProgressBar();
            ViewModel.GetPosition();
        }

        public void OnSuccessLocation()
        {
            HideProgressBar();
            FiscalUrnaCheckinView.DataContext = ViewModel.GetData();
            
            FiscalUrnaCheckinView.Map.Center = ViewModel.Coordinate;
            new MapUtil(FiscalUrnaCheckinView.Map).AddCustomMarker(ViewModel.Coordinate);
        }

        public void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "Localização", MessageBoxButton.OK);
        }
    }
}