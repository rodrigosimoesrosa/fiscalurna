﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.ViewModel;
using System.Windows.Media;
using FiscalUrna.Util;

namespace FiscalUrna.Page.Sign
{
    public partial class FiscalUrnaSignPage : PhoneApplicationPage
    {
        public ProgressIndicator Progress { get; set; }

        public FiscalUrnaSignViewModel ViewModel { get; set; }

        public FiscalUrnaSignPage()
        {
            InitializeComponent();
            ViewModel = new FiscalUrnaSignViewModel(this);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string edit = string.Empty;
            if(NavigationContext.QueryString.TryGetValue("edit", out edit))
            {
                ShowEdit();
            }  
        }

        private void ShowEdit()
        {
            DataContext = ViewModel.GetUser();

            FiscalUrnaEditView.Visibility = System.Windows.Visibility.Visible;
            FiscalUrnaSignView.Visibility = System.Windows.Visibility.Collapsed;
            LblTitle.Text = "Usuário";
        }

        public void ShowLoading()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Realizando o cadastro ...";

            SystemTray.SetProgressIndicator(this, Progress);

            FiscalUrnaSignView.BeforeRegister();
        }

        public void HideProgressBar()
        {
            SystemTray.SetIsVisible(this, false);
            Progress.Text = string.Empty;

            FiscalUrnaSignView.AfterRegister();
        }

        public void OnSign(string mail, string password, string name)
        {
            ViewModel.Register(mail, password, name);

            ShowLoading();
        }

        public void OnAccept()
        {
            NavigationService.Navigate(new Uri("/Page/Home/FiscalUrnaHomePage.xaml", UriKind.RelativeOrAbsolute));
        }

        public void OnRefused(string error)
        {
            MessageBoxResult result = MessageBox.Show("Ocorreu um erro no registro do usuário!",
                                   "Cadastro", MessageBoxButton.OK);
        }

        public void OnRemoveUser()
        {
            MessageBoxResult result = MessageBox.Show("Deseja remover este usuário do app?", "Alerta", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                ViewModel.OnRemoveUser();
                NavigationService.GoBack();
            }
        }

        public void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "Alerta", MessageBoxButton.OK); 
        }
    }
}