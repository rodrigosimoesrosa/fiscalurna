﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FiscalUrna.Page.Util
{
    public interface HandlerDownload<T>
    {
        void OnFinishDownload(T objects);
        void OnErrorDownload(string error);
    }
}
