﻿using Coding4Fun.Toolkit.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FiscalUrna.Page.Util
{
    public class ToastManager
    {
        public static ToastPrompt GetNewToast(string title, string message)
        {
            return new ToastPrompt
            {
                Title = title,
                TextWrapping = System.Windows.TextWrapping.Wrap,
                TextOrientation = System.Windows.Controls.Orientation.Vertical,
                Message = message,
            };
        }

        public static ToastPrompt GetNewToastImage(string title,string message)
        {
            return new ToastPrompt
            {
                Title = title,
                TextWrapping = System.Windows.TextWrapping.Wrap,
                TextOrientation = System.Windows.Controls.Orientation.Vertical,
                Message = message,
                ImageSource = new BitmapImage(new Uri("..\\ApplicationIcon.png", UriKind.RelativeOrAbsolute)),
            };
        }
    }
}
