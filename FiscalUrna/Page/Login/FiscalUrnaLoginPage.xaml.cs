﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.ViewModel.Login;
using System.Windows.Media;

namespace FiscalUrna.Page.Login
{
    public partial class FiscalUrnaLoginPage : PhoneApplicationPage
    {
        private ProgressIndicator Progress;

        public FiscalUrnaLoginViewModel ViewModel { get; set; }

        public FiscalUrnaLoginPage()
        {
            InitializeComponent();
            ViewModel = new FiscalUrnaLoginViewModel(this);
        }

        public void OnRegister()
        {
            NavigationService.Navigate(new Uri("/Page/Sign/FiscalUrnaSignPage.xaml", UriKind.RelativeOrAbsolute));            
        }

        public void OnSign(string mail, string password)
        {
            ShowProgressBar();
            ViewModel.Login(mail,password);
        }

        public void OnFail(string message)
        {
            HideProgressBar();
            MessageBoxResult result = MessageBox.Show(message, "Alerta", MessageBoxButton.OK);
        }

        public void OnSuccess()
        {
            HideProgressBar();
            NavigationService.GoBack();
        }

        public void ShowProgressBar()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Carregando ...";

            SystemTray.SetProgressIndicator(this, Progress);

            FiscalUrnaLoginView.BeforeLogin();
        }

        public void HideProgressBar()
        {
            SystemTray.SetIsVisible(this, false);
            Progress.Text = string.Empty;

            FiscalUrnaLoginView.AfterLogin();
        }

        public void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "Login", MessageBoxButton.OK);
        }
    }
}