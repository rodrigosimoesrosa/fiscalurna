﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.ViewModel.Terms;

namespace FiscalUrna.Page.Terms
{
    public partial class FiscalUrnaTermsUsePage : PhoneApplicationPage
    {
        public FiscalUrnaTermsUseViewModel ViewModel { get; set; }

        public FiscalUrnaTermsUsePage()
        {
            InitializeComponent();
            ViewModel = new FiscalUrnaTermsUseViewModel(this);
            Loaded += OnLoadedPage;
        }

        private void OnLoadedPage(object sender, RoutedEventArgs e)
        {
            FiscalUrnaTermsUseView.CheckAcceptTerms.IsChecked = ViewModel.IsAcceptTerms();
        }

        public void AcceptTerms()
        {
            ViewModel.AcceptTerms();
        }

        public void RefuseTerms()
        {
            ViewModel.RefuseTerms();
        }
    }
}