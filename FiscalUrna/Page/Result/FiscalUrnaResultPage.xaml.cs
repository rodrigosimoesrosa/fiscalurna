﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.ViewModel.Result;
using FiscalUrna.View.Util.Map;
using FiscalUrna.Model;
using FiscalUrna.Util.Image;
using System.Windows.Media;

namespace FiscalUrna.Page
{
    public partial class FiscalUrnaResultPage : PhoneApplicationPage
    {
        private ProgressIndicator Progress;

        public FiscalUrnaResultViewModel ViewModel { get; set; }

        public FiscalUrnaResultPage()
        {
            InitializeComponent();

            ViewModel = new FiscalUrnaResultViewModel(this);
            Loaded += new RoutedEventHandler(OnLoadedPage);
        }

        private void OnLoadedPage(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();
        }

        public void ShowProgressBar()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Publicando sua denuncia ...";

            SystemTray.SetProgressIndicator(this, Progress);

            FiscalUrnaResultView.BeforeCheckin();
        }

        public void HideProgressBar()
        {
            SystemTray.SetIsVisible(this, false);
            Progress.Text = string.Empty;

            FiscalUrnaResultView.AfterPublish();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Complain data = ViewModel.GetData();
            FiscalUrnaResultView.DataContext = data;

            if (data.Bitmap == null)
            {
                FiscalUrnaResultView.ImgIcon.Source = ImageUtil.GetWithoutImage();
                FiscalUrnaResultView.ImgResult.Visibility = System.Windows.Visibility.Collapsed;
            }

            new MapUtil(FiscalUrnaResultView.Map).AddCustomMarker(ViewModel.GetData().Coordinate);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Você irá perder as informações da denuncia. Deseja continuar ?", "Denuncia", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }

            base.OnBackKeyPress(e);
        }

        public void OnPublish()
        {
            ShowProgressBar();
            ViewModel.Publish();
        }

        public void OnPublishSuccess()
        {
            HideProgressBar();
            MessageBox.Show("Sua denuncia foi publicada com sucesso !", "Denuncia", MessageBoxButton.OK);

            NavigationService.GoBack();
        }

        internal void OnPublishFail(string message)
        {
            HideProgressBar();
            MessageBox.Show(message, "Denuncia", MessageBoxButton.OK);
        }
    }
}