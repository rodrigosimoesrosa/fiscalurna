﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Resources;
using FiscalUrna.Model;
using System.Windows.Media;
using FiscalUrna.ViewModel.Main;
using FiscalUrna.Page.Util;

namespace FiscalUrna
{
    public partial class MainPage : PhoneApplicationPage
    {
        private ProgressIndicator Progress { get; set; }

        public MainViewModel ViewModel {get;set;}

        public MainPage()
        {
            InitializeComponent();

            ViewModel = new MainViewModel(this);
            ViewModel.LoadData();

            ShowLoading();
        }

        public void ShowLoading()
        {
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetOpacity(this, 0.5);
            SystemTray.SetBackgroundColor(this, Colors.Orange);
            SystemTray.SetForegroundColor(this, Colors.Black);

            Progress = new ProgressIndicator();
            Progress.IsVisible = true;
            Progress.IsIndeterminate = true;
            Progress.Text = "Carregando informações ...";

            SystemTray.SetProgressIndicator(this, Progress);
        }

        public void HideLoading()
        {
            SystemTray.SetIsVisible(this, false);
        }

        public void OnFail()
        {
            HideLoading();
            OnNext();
        }

        public void OnSuccess()
        {
            HideLoading();
            OnNext();
        }

            public void OnNext()
        {
            NavigationService.Navigate(new Uri("/Page/Home/FiscalUrnaHomePage.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}