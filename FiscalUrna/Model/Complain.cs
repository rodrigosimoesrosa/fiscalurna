﻿using Microsoft.Phone.Maps.Services;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FiscalUrna.Model
{
    public class Complain
    {
        public static string ELECTORAL_GARBAGE { get { return "Lixo Eleitoral"; } }

        public static string EXIT_POLLS { get { return "Boca de Urna"; } }

        public string Type { get; set; }

        public int Id { get; set; }

        public GeoCoordinate Coordinate { get; set; }

        public MapAddress Address { get; set; }

        public BitmapImage Bitmap { get; set; }

        public DateTime Time { get; set; }

        public string Description { get; set; }

        public string Date { get { return String.Format("{0:dd/M/yyyy HH:mm:ss}", Time); } }

        public string Street { get { return Address == null ? "" : Address.Street; } }

        public string City { get { return Address == null ? "" : Address.City; } }

        public string State { get { return Address == null ? "" : Address.State; } }

        public string HouseNumber { get { return  Address == null ? "" : Address.HouseNumber; } }

        public Complain(string type)
        {
            Type = type;
        }

        public Complain Clone()
        {
            Complain clone = MemberwiseClone() as Complain;
            clone.Coordinate = Coordinate;
            clone.Address = Address;
            clone.Bitmap = Bitmap;
            clone.Time = Time;

            return clone;
        }
    }


}
