﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Terms;
using FiscalUrna.View.Util;
using System.Diagnostics;

namespace FiscalUrna.View.Terms
{
    public partial class FiscalUrnaTermsUseView : UserControl, UserControlPage<FiscalUrnaTermsUsePage>
    {
        
        public FiscalUrnaTermsUseView()
        {
            InitializeComponent();
        }

        public FiscalUrnaTermsUsePage GetPage()
        {
            return ((FiscalUrnaTermsUsePage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnCheckAcceptTerms(object sender, RoutedEventArgs e)
        {
            if (CheckAcceptTerms.IsChecked == true)
            {
                GetPage().AcceptTerms();
            }
            else
            {
                GetPage().RefuseTerms();
            }
        }
    }
}
