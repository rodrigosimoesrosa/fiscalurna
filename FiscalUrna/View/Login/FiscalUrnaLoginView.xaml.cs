﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Login;
using FiscalUrna.View.Util;
using System.Text.RegularExpressions;

namespace FiscalUrna.View.Login
{
    public partial class FiscalUrnaLoginView : UserControl, UserControlPage<FiscalUrnaLoginPage>
    {

        public FiscalUrnaLoginView()
        {
            InitializeComponent();
        }

        public FiscalUrnaLoginPage GetPage()
        {
            return ((FiscalUrnaLoginPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnSignClick(object sender, RoutedEventArgs e)
        {
            if (ValidateUtil.IsValidMail(TxtMail.Text))
            {
                if (IsValidPassword())
                {
                    GetPage().OnSign(TxtMail.Text, TxtPassword.Password);
                }
                else
                {
                    GetPage().ShowMessageBox("O campo da senha não foi preenchido!");
                }
            }
            else
            {
                GetPage().ShowMessageBox("O campo email não é valido!");
            }
            
        }

        private bool IsValidPassword()
        {
            return TxtPassword.Password != string.Empty;
        }

        private void OnRegisterClick(object sender, RoutedEventArgs e)
        {
            GetPage().OnRegister();
        }

        public void BeforeLogin()
        {
            TxtMail.IsEnabled = false;
            TxtPassword.IsEnabled = false;
            BtnRegistrar.IsEnabled = false;
            BtnSign.IsEnabled = false;
        }

        public void AfterLogin()
        {
            TxtMail.IsEnabled = true;
            TxtPassword.IsEnabled = true;
            BtnRegistrar.IsEnabled = true;
            BtnSign.IsEnabled = true;
        }
    }
}
