﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Photo;
using FiscalUrna.View.Util;
using System.Windows.Media.Imaging;

namespace FiscalUrna.View.Photo
{
    public partial class FiscalUrnaPhotoView : UserControl , UserControlPage<FiscalUrnaPhotoPage>
    {
       public FiscalUrnaPhotoView()
        {
            InitializeComponent();
        }

       private void OnChangeImageClick(object sender, RoutedEventArgs e)
       {
           GetPage().CapturePhoto();
       }

       private void OnNextClick(object sender, RoutedEventArgs e)
       {
           GetPage().NextPage();
       }

       public FiscalUrnaPhotoPage GetPage()
       {
           return ((FiscalUrnaPhotoPage) ((PhoneApplicationFrame)Application.Current.RootVisual).Content);
       }

       public void SetImage(BitmapImage Bitmap)
       {
           Image.Source = Bitmap;
           Image.Stretch = System.Windows.Media.Stretch.Uniform;
       }
    }
}
