﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Details;
using FiscalUrna.View.Util;

namespace FiscalUrna.View.Details
{
    public partial class FiscalUrnaDetailsView : UserControl, UserControlPage<FiscalUrnaDetailsComplainPage>
    {
        public FiscalUrnaDetailsView()
        {
            InitializeComponent();
        }

        public FiscalUrnaDetailsComplainPage GetPage()
        {
            return ((FiscalUrnaDetailsComplainPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        public void After()
        {
            StackField.Visibility = System.Windows.Visibility.Visible;
            Map.Visibility = System.Windows.Visibility.Visible;
        }

        public void Before()
        {
            StackField.Visibility = System.Windows.Visibility.Collapsed;
            Map.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
