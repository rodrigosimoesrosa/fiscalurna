﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Home;
using FiscalUrna.View.Util;
using FiscalUrna.Model;

namespace FiscalUrna.View.Home
{
    public partial class FiscalUrnaMyComplainView : UserControl, UserControlPage<FiscalUrnaHomePage>
    {
        public FiscalUrnaMyComplainView()
        {
            InitializeComponent();
        }

        private void OnSelectComplain(object sender, SelectionChangedEventArgs e)
        {
            if (ListBox.SelectedIndex != -1)
            {
                Complain c = (Complain)ListBox.SelectedItem;
                GetPage().ShowDetailsComplain(c.Id);
            }
        }

        public FiscalUrnaHomePage GetPage()
        {
            return ((FiscalUrnaHomePage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }
    }
}
