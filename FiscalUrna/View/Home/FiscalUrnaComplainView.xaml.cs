﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Util;
using FiscalUrna.Model;
using FiscalUrna.Page.Home;
using FiscalUrna.View.Util;

namespace FiscalUrna.View.Home
{
    public partial class FiscalUrnaComplainView : UserControl, UserControlPage<FiscalUrnaHomePage>
    {
        public FiscalUrnaComplainView()
        {
            InitializeComponent();
        }

        public FiscalUrnaHomePage GetPage()
        {
            return ((FiscalUrnaHomePage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnExitPollsClick(object sender, RoutedEventArgs e)
        {
            GetPage().DoComplain(Complain.EXIT_POLLS);
        }

        private void OnElectoralGarbageClick(object sender, RoutedEventArgs e)
        {
            GetPage().DoComplain(Complain.ELECTORAL_GARBAGE);
        }
    }
}
