﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.Page.Sign;
using FiscalUrna.View.Util;

namespace FiscalUrna.View.Sign
{
    public partial class FiscalUrnaEditView : UserControl, UserControlPage<FiscalUrnaSignPage>
    {
        public FiscalUrnaEditView()
        {
            InitializeComponent();
        }

        public FiscalUrnaSignPage GetPage()
        {
            return ((FiscalUrnaSignPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnRemoveUser(object sender, RoutedEventArgs e)
        {
            GetPage().OnRemoveUser();
        }
    }
}
