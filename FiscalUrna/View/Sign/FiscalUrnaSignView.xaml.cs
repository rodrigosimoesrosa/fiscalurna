﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FiscalUrna.View.Util;
using FiscalUrna.Page;
using FiscalUrna.Page.Sign;

namespace FiscalUrna.View.Sign
{
    public partial class FiscalUrnaSignView : UserControl, UserControlPage<FiscalUrnaSignPage>
    {
        public FiscalUrnaSignView()
        {
            InitializeComponent();
        }

        public FiscalUrnaSignPage GetPage()
        {
            return ((FiscalUrnaSignPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnSignClick(object sender, RoutedEventArgs e)
        {
            if (ValidateUtil.IsValidMail(TxtMail.Text))
            {
                if(IsValidPassword())
                {
                    if (IsValidName())
                    {
                        GetPage().OnSign(TxtMail.Text, TxtPassword.Password, TxtName.Text);
                    }
                    else
                    {
                        GetPage().ShowMessageBox("O campo do nome não foi preenchido!");
                    }
                }
                else
                {
                    GetPage().ShowMessageBox("O campo da senha não foi preenchido!");
                }
            } 
            else 
            {
                GetPage().ShowMessageBox("O campo email não é valido!");
            }
        }

        private bool IsValidName()
        {
            return TxtName.Text != string.Empty;
        }

        private bool IsValidPassword()
        {
            return TxtPassword.Password != string.Empty;
        }

        public void AfterRegister()
        {
            TxtMail.IsEnabled = true;
            TxtName.IsEnabled = true;
            TxtPassword.IsEnabled = true;

            BtnSign.IsEnabled = true;
        }

        public void BeforeRegister()
        {
            TxtMail.IsEnabled = false;
            TxtName.IsEnabled = false;
            TxtPassword.IsEnabled = false;

            BtnSign.IsEnabled = false;
        }
    }
}
