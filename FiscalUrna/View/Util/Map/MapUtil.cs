﻿using Microsoft.Phone.Maps.Controls;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Windows.Devices.Geolocation;

namespace FiscalUrna.View.Util.Map
{
    public class MapUtil
    {
        public Microsoft.Phone.Maps.Controls.Map Map { get; set; }
        public MouseButtonEventHandler MouseHandlerMarker { get; set; }
        public Color Red { get { return Colors.Red; } }
        public GeoCoordinate Coordinate { get; set; }

        public Geoposition Position { get; set; }

        private double accuracy = 0.0;

        public MapUtil(Microsoft.Phone.Maps.Controls.Map map)
        {
            Map = map;
        }

        public void AddMapMarker(GeoCoordinate coordinate, Geoposition position, MouseButtonEventHandler handler)
        {
            if (coordinate != null && position != null)
            {
                Coordinate = coordinate;
                Position = position;
                accuracy = position.Coordinate.Accuracy;
                Map.Layers.Clear();
                MapLayer layer = new MapLayer();
                MouseHandlerMarker = handler;

                DrawAccuracyRadius(layer);
                DrawMapMarker(coordinate, layer);

                Map.Layers.Add(layer);
            }
        }

        public void AddCustomMarker(GeoCoordinate coordinate)
        {
            if (coordinate != null)
            {
                Coordinate = coordinate;
                Map.Layers.Clear();
                MapLayer layer = new MapLayer();

                DrawCustomMapMarker(coordinate, layer);
                Map.Layers.Add(layer);
            }
        }

        private void DrawCustomMapMarker(GeoCoordinate coordinate, MapLayer mapLayer)
        {
            FiscalUrnaMapMarker marker = new FiscalUrnaMapMarker();
            
            MapOverlay overlay = new MapOverlay();
            overlay.Content = marker;
            overlay.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            overlay.PositionOrigin = new Point(0.0, 1.0);
            mapLayer.Add(overlay);
        }

        private void DrawMapMarker(GeoCoordinate coordinate, MapLayer mapLayer)
        {
            Polygon polygon = new Polygon();
            polygon.Points.Add(new Point(0, 0));
            polygon.Points.Add(new Point(0, 75));
            polygon.Points.Add(new Point(25, 0));
            polygon.Fill = new SolidColorBrush(Red);

            polygon.Tag = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            polygon.MouseLeftButtonUp += new MouseButtonEventHandler(MouseHandlerMarker);

            MapOverlay overlay = new MapOverlay();
            overlay.Content = polygon;
            overlay.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            overlay.PositionOrigin = new Point(0.0, 1.0);
            mapLayer.Add(overlay);
        }

        private void DrawAccuracyRadius(MapLayer mapLayer)
        {
            double metersPerPixels = (Math.Cos(Coordinate.Latitude * Math.PI / 180) * 2 * Math.PI * 6378137) / (256 * Math.Pow(2, Map.ZoomLevel));
            double radius = accuracy / metersPerPixels;

            Ellipse ellipse = new Ellipse();
            ellipse.Width = radius * 1.2;
            ellipse.Height = radius * 1.2;
            ellipse.Fill = new SolidColorBrush(Color.FromArgb(20, 200, 0, 0));

            MapOverlay overlay = new MapOverlay();
            overlay.Content = ellipse;
            overlay.GeoCoordinate = Coordinate;
            overlay.PositionOrigin = new Point(0.0, 0.0);
            mapLayer.Add(overlay);
        }
    }
}



    

