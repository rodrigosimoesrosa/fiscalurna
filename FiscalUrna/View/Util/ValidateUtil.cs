﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FiscalUrna.View.Util
{
    public class ValidateUtil
    {
        public static bool IsValidMail(string mail)
        {
            Match match = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(mail);
            return match.Success;
        }
    }
}
