﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiscalUrna.View.Util
{
    public interface UserControlPage<T> where T : PhoneApplicationPage
    {
        T GetPage();
    }
}
