﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Windows.Devices.Geolocation;
using System.Windows.Media;
using FiscalUrna.Util;
using Microsoft.Phone.Maps.Services;
using System.Device.Location;
using FiscalUrna.Page.Checkin;
using FiscalUrna.View.Util;

namespace FiscalUrna.View.Checkin
{
    public partial class FiscalUrnaCheckinView : UserControl, UserControlPage<FiscalUrnaCheckinPage>
    {
        public FiscalUrnaCheckinView()
        {
            InitializeComponent();
        }

        public void BeforeCheckin()
        {
            StackField.Visibility = System.Windows.Visibility.Collapsed;

            Map.Visibility = System.Windows.Visibility.Collapsed;
            
            BtnNext.Visibility = System.Windows.Visibility.Collapsed;
            BtnCheckin.IsEnabled = false;
        }

        public void AfterCheckin()
        {
            StackField.Visibility = System.Windows.Visibility.Visible;

            BtnCheckin.Height = 170;

            Map.Visibility = System.Windows.Visibility.Visible;
            BtnNext.Visibility = System.Windows.Visibility.Visible;
            BtnCheckin.IsEnabled = true;
        }

        private void OnCheckinClick(object sender, RoutedEventArgs e)
        {
            GetPage().CheckinClick();
        }

        private void OnNextClick(object sender, RoutedEventArgs e)
        {
            if (TxtDescription.Text.Trim() == string.Empty)
            {
                GetPage().ShowMessageBox("O campo descrição deve ser preenchido corretamente");
            }
            else
            {
                GetPage().NextPage();
            }
        }

        public FiscalUrnaCheckinPage GetPage()
        {
            return ((FiscalUrnaCheckinPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }
    }
}
