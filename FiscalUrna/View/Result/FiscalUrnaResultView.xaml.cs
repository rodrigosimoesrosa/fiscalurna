﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using FiscalUrna.Util;
using System.Device.Location;
using System.Windows.Media.Imaging;
using FiscalUrna.Page;
using FiscalUrna.View.Util;


namespace FiscalUrna.View.Result
{
    public partial class FiscalUrnaResultView : UserControl, UserControlPage<FiscalUrnaResultPage>
    {
        public FiscalUrnaResultView()
        {
            InitializeComponent();
        }

        public FiscalUrnaResultPage GetPage()
        {
            return ((FiscalUrnaResultPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content);
        }

        private void OnPublish(object sender, RoutedEventArgs e)
        {
            GetPage().OnPublish(); 
        }

        public void BeforeCheckin()
        {
            BtnPublish.IsEnabled = false;
        }

        public void AfterPublish()
        {
            BtnPublish.IsEnabled = true;
        }
    }
}
