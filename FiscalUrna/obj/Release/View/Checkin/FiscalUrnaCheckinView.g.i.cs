﻿#pragma checksum "E:\development\projects\windowsphone\mirabilis\FiscalUrna\FiscalUrna\View\Checkin\FiscalUrnaCheckinView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "332A032FF532204A24D92B700DC8B597"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Maps.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace FiscalUrna.View.Checkin {
    
    
    public partial class FiscalUrnaCheckinView : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.StackPanel LayoutRoot;
        
        internal System.Windows.Controls.Button BtnCheckin;
        
        internal System.Windows.Controls.Image ImgBtn;
        
        internal System.Windows.Controls.StackPanel StackField;
        
        internal System.Windows.Controls.TextBlock LblState;
        
        internal System.Windows.Controls.TextBlock TxtState;
        
        internal System.Windows.Controls.TextBlock LblCity;
        
        internal System.Windows.Controls.TextBlock TxtCity;
        
        internal System.Windows.Controls.TextBlock LblStreet;
        
        internal System.Windows.Controls.TextBlock TxtStreet;
        
        internal System.Windows.Controls.TextBlock LblHouseNumber;
        
        internal System.Windows.Controls.TextBlock TxtHouseNumber;
        
        internal System.Windows.Controls.TextBlock LblDescription;
        
        internal System.Windows.Controls.TextBox TxtDescription;
        
        internal Microsoft.Phone.Maps.Controls.Map Map;
        
        internal System.Windows.Controls.Button BtnNext;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/FiscalUrna;component/View/Checkin/FiscalUrnaCheckinView.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.StackPanel)(this.FindName("LayoutRoot")));
            this.BtnCheckin = ((System.Windows.Controls.Button)(this.FindName("BtnCheckin")));
            this.ImgBtn = ((System.Windows.Controls.Image)(this.FindName("ImgBtn")));
            this.StackField = ((System.Windows.Controls.StackPanel)(this.FindName("StackField")));
            this.LblState = ((System.Windows.Controls.TextBlock)(this.FindName("LblState")));
            this.TxtState = ((System.Windows.Controls.TextBlock)(this.FindName("TxtState")));
            this.LblCity = ((System.Windows.Controls.TextBlock)(this.FindName("LblCity")));
            this.TxtCity = ((System.Windows.Controls.TextBlock)(this.FindName("TxtCity")));
            this.LblStreet = ((System.Windows.Controls.TextBlock)(this.FindName("LblStreet")));
            this.TxtStreet = ((System.Windows.Controls.TextBlock)(this.FindName("TxtStreet")));
            this.LblHouseNumber = ((System.Windows.Controls.TextBlock)(this.FindName("LblHouseNumber")));
            this.TxtHouseNumber = ((System.Windows.Controls.TextBlock)(this.FindName("TxtHouseNumber")));
            this.LblDescription = ((System.Windows.Controls.TextBlock)(this.FindName("LblDescription")));
            this.TxtDescription = ((System.Windows.Controls.TextBox)(this.FindName("TxtDescription")));
            this.Map = ((Microsoft.Phone.Maps.Controls.Map)(this.FindName("Map")));
            this.BtnNext = ((System.Windows.Controls.Button)(this.FindName("BtnNext")));
        }
    }
}

