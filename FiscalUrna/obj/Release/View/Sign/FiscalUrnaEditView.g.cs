﻿#pragma checksum "E:\development\projects\windowsphone\mirabilis\FiscalUrna\FiscalUrna\View\Sign\FiscalUrnaEditView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8C4F1F039A8B95E4F359DE4EA07B2ED5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace FiscalUrna.View.Sign {
    
    
    public partial class FiscalUrnaEditView : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.StackPanel LayoutRoot;
        
        internal System.Windows.Controls.TextBlock LblMail;
        
        internal System.Windows.Controls.TextBlock TxtMail;
        
        internal System.Windows.Controls.TextBlock LblName;
        
        internal System.Windows.Controls.TextBlock TxtName;
        
        internal System.Windows.Controls.Button BtnRemove;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/FiscalUrna;component/View/Sign/FiscalUrnaEditView.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.StackPanel)(this.FindName("LayoutRoot")));
            this.LblMail = ((System.Windows.Controls.TextBlock)(this.FindName("LblMail")));
            this.TxtMail = ((System.Windows.Controls.TextBlock)(this.FindName("TxtMail")));
            this.LblName = ((System.Windows.Controls.TextBlock)(this.FindName("LblName")));
            this.TxtName = ((System.Windows.Controls.TextBlock)(this.FindName("TxtName")));
            this.BtnRemove = ((System.Windows.Controls.Button)(this.FindName("BtnRemove")));
        }
    }
}

